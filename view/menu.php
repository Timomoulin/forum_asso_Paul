    <!-- HEADER -->
    <header role="banner" class="position-absolute">
      <!-- Top Navigation -->
      <nav class="background-transparent background-transparent-hightlight full-width fixed sticky">
        <div class="s-12 l-2">
          <a href="index.html" class="logo">
            <!-- Logo White Version -->
            <img class="logo-white" src="./public/img/logo.jpg" alt="">
            <!-- Logo Dark Version -->
            <img class="logo-dark" src="./public/img/logo-dark.jpg" alt="">
          </a>
        </div>
        <div class="top-nav s-12 l-10">

          <ul class="right chevron">
            <li><a href="./">Accueil</a></li>
            <li><a href="./?action=Types">Les Types d'associations</a></li>
            <li><a>Services</a>
              <ul>
                <li><a>Adhérent</a>
                  <ul>
                    <li><a>Déjà inscrit</a></li>
                    <li><a>Pas encore inscrit</a></li>
                  </ul>
                </li>
                <li><a>Vous êtes une association</a></li>
              </ul>
            </li>
            <li><a href="./?action=about">A propos</a></li>
            <li><a href="./?action=gallery">Galerie</a></li>
            <li><a href="./?action=contact">Contact</a></li>
          </ul>
        </div>
      </nav>
    </header>

