<?php $title = 'A propos'; ?>

<?php ob_start(); ?>
    <!-- MAIN -->
    <main role="main">
      <!-- Content -->
      <article>
        <header class="section background-dark">
          <div class="line">
            <h1 class="text-white margin-top-bottom-40 text-size-60 text-line-height-1">Le forum des associations</h1>
            <p class="margin-bottom-0 text-size-16">Le forum des associations en ligne nous permet de préserver la vie associative tout en respectant les règles de distanciation en cette période de pandémie.</p>
          </div>
        </header>
        <div class="section background-white">
          <div class="line">
            <h2 class="text-size-40 margin-bottom-30">OFFICE MUNICIPAL DE LA CULTURE (OMC)</h2>
            <hr class="break-small background-primary margin-bottom-30">
            <p>
              L’Office Municipal de la Culture, des Loisirs et de la Jeunesse de Maisons Alfort est une association régie par la loi du 1er juillet 1901.
              <br>
              <br>
  Il a été créé en 1971 avec pour objet de promouvoir, soutenir et favoriser à
  Maisons-Alfort la création et le développement de toutes les activités d’ordre
  social, culturel, de loisirs et d’éducation pour les jeunes et les adultes.
  Afin de remplir ses missions, l’OMC dispose de 10 équipements sur les
  différents quartiers de la ville.</p>

  <p>
  Ces équipements de quartiers sont des structures de proximité, ouvertes à
  tous, portant les valeurs de respect, d’échanges, d’écoute et favorisant la
  mixité et l’intégration.</p>

  <br>
  <p>
  LEURS MISSIONS SONT :
  <br>
  <br>
  - d’accueillir, de proposer des ateliers et des services aux habitants, en portant
  une attention particulière aux individus et familles fragilisés.
<br>
<br>
  - d’accueillir les 70 associations affiliées à l’OMC qui dispensent toute l’année
  des activités de loisirs, de culture et socioculturelles aux maisonnais.
<br>
<br>
  - d’être des lieux de fédération sur des projets collectifs avec les associations
  affiliées et les bénévoles
  </p>
  <br>
  <p>LES ATELIERS POUR ADULTES visent la créativité, l’entraide, le partage et
  le lien social.
  <br>
  <br>
  L’ACCOMPAGNEMENT A LA SCOLARITE participe à la réussite scolaire et à
  la découverte de nouveaux apprentissages.
  <br>
  <br>
  LES LUDOTHEQUES invitent tous les habitants à jouer sur place ou à
  emprunter des jeux pour jouer en famille ou entre amis.
  <br><br>
  LES ACCUEILS ENFANTS, ADOLESCENTS ET JEUNES favorisent l’éveil et
  développent des démarches citoyennes.
  <br><br>
  INFO-SENIORS informe les retraités et leur propose des activités et de
  nombreuses animations et sorties.
  <br><br>
  INFO-PARENTS propose un lieu d’accueil et d’échanges autour de la
  parentalité pour tous les adultes maisonnais.
  <br><br>
  DES PERMANENTS ET DES BENEVOLES, motivés et professionnels,
  répondent aux besoins des habitants en inscrivant leur action sur des champs
  de compétences spécifiques (échecs, accompagnement à la scolarité, activités
  artistiques…).
  <br><br>
  Outre les locaux de sa direction qui sont situés 120 rue Roger François
  (au-dessus du marché de Charentonneau), l’OMC gère 10 équipements de
  quartier.
            </p>
            <blockquote class="margin-top-bottom-20">
              <h3 class="text-size-20 text-line-height-1 margin-bottom-15">OFFICE MUNICIPAL DES SPORTS
(OMS)
</h3>
              L’Office Municipal des Sports est un carrefour, où se rassemblent toutes
              celles et ceux qui, dans la cité, exercent une responsabilité dans le domaine
              de l’éducation physique et sportive, du sport en général et des loisirs à
              caractère sportif.
            </blockquote>

            <div class="line margin-top-30">
              <div class="margin">
                <div class="s-12 m-6 l-6">
                  <img src="img/img1.png" alt="">
                  <p class="margin-top-30">
                    Les statuts de l’Office Municipal des Sports (OMS) ont été adoptés le 25 juin 1959.
                    L’OMS dont le Président est le Maire de Maisons-Alfort, est constitué sous forme
                    d’association Loi 1901.
                    <br><br>
                    Son bureau comprend à parité des représentants du Conseil Municipal et des
                    représentants des associations sportives de la ville.

                  </p>
                </div>
                <div class="s-12 m-6 l-6">
                  <img src="img/img2.png" alt="">
                  <p class="margin-top-30">
                    LE ROLE DE L’OMS S’ARTICULE AUTOUR DE PLUSIEURS
                    THEMES :
                    <br><br>
                    - L’ACTION AU SERVICE DES ASSOCIATIONS, DE LA JEUNESSE ET DES
                    SPORTIFS MAISONNAIS en favorisant le développement de toutes les activités
                    sportives pour les jeunes et les adultes,
                    <br><br>
                    - LE SOUTIEN DE LA VIE ASSOCIATIVE et la diffusion des résultats sportifs,
                    <br><br>
                    - L’ORGANISATION ET LA PROMOTION D’EVENEMENTS SPORTIFS,
                    <br><br>
                    - LA DEFENSE DE L’ETHIQUE DU SPORT ET DU FAIR-PLAY, à travers la
                    promotion de l’esprit sportif, basé sur la loyauté, la générosité et la tolérance.
                    Une trentaine d’associations ou de sections sportives est affiliée à l’OMS.
                    CES ASSOCIATIONS REGROUPENT PRES DE 7 000
                    ADHERENTS,
                    LICENCIES AUPRES DES FEDERATIONS SPORTIVES.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </article>
    </main>
<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>