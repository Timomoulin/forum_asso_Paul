<?php $title = 'Home'; ?>

<?php ob_start(); ?>
    <!-- MAIN -->
    <main role="main">
      <!-- Content -->
      <article>
        <header class="section background-dark">
          <div class="line">
            <h1 class="text-white margin-top-bottom-40 text-size-60 text-line-height-1">Galerie d'images</h1>
            <p class="margin-bottom-0 text-size-16">Voici quelques images qui pourront vous donner une idée des activités proposées par les associations de Maisons Alfort.</p>
          </div>
        </header>

        <div class="section background-white">
          <div class="line">
            <div class="margin">
              <div class="s-12 m-6 l-4">
                <div class="image-with-hover-overlay image-hover-zoom margin-bottom">
                  <div class="image-hover-overlay background-primary">
                    <div class="image-hover-overlay-content text-center padding-2x">
                      <p>Gym douce adaptée à tous les âges</p>
                    </div>
                  </div>
                  <img src="./public/img/portfolio/thumb-01.jpg" alt="" title="Portfolio Image 1" />
                </div>
              </div>
              <div class="s-12 m-6 l-4">
                <div class="image-with-hover-overlay image-hover-zoom margin-bottom">
                  <div class="image-hover-overlay background-primary">
                    <div class="image-hover-overlay-content text-center padding-2x">
                      <p>Le Jibengong signifie compétences de base ou fondamentaux. Remise en forme et en souplesse.</p>
                    </div>
                  </div>
                  <img src="./public/img/portfolio/thumb-02.jpg" alt="" title="Portfolio Image 2" />
                </div>
              </div>
              <div class="s-12 m-6 l-4">
                <div class="image-with-hover-overlay image-hover-zoom margin-bottom">
                  <div class="image-hover-overlay background-primary">
                    <div class="image-hover-overlay-content text-center padding-2x">
                      <p>La danse française sa forme la plus aboutie : le ballet classique.</p>
                    </div>
                  </div>
                  <img src="./public/img/portfolio/thumb-03.jpg" alt="" title="Portfolio Image 3" />
                </div>
              </div>
              <div class="s-12 m-6 l-4">
                <div class="image-with-hover-overlay image-hover-zoom margin-bottom">
                  <div class="image-hover-overlay background-primary">
                    <div class="image-hover-overlay-content text-center padding-2x">
                      <p>La danse jazz est une expression générique autant des danses de société que des danses théâtrales.</p>
                    </div>
                  </div>
                  <img src="./public/img/portfolio/thumb-04.jpg" alt="" title="Portfolio Image 4" />
                </div>
              </div>
              <div class="s-12 m-6 l-4">
                <div class="image-with-hover-overlay image-hover-zoom margin-bottom">
                  <div class="image-hover-overlay background-primary">
                    <div class="image-hover-overlay-content text-center padding-2x">
                      <p>La danse aujourd'hui nommée danse contemporaine fait suite à la danse moderne et débute, pour certains, avec les courants postmodernistes.</p>
                    </div>
                  </div>
                  <img src="./public/img/portfolio/thumb-06.jpg" alt="" title="Portfolio Image 5" />
                </div>
              </div>
              <div class="s-12 m-6 l-4">
                <div class="image-with-hover-overlay image-hover-zoom margin-bottom">
                  <div class="image-hover-overlay background-primary">
                    <div class="image-hover-overlay-content text-center padding-2x">
                      <p>La gymnastique artistique est une discipline athlétique consistant à enchaîner des mouvements acrobatiques sur des agrès.</p>
                    </div>
                  </div>
                  <img src="./public/img/portfolio/thumb-11.jpg" alt="" title="Portfolio Image 6" />
                </div>
              </div>
              <div class="s-12 m-6 l-4">
                <div class="image-with-hover-overlay image-hover-zoom margin-bottom">
                  <div class="image-hover-overlay background-primary">
                    <div class="image-hover-overlay-content text-center padding-2x">
                      <p>Techniques de défense, clés aux articulations, esquives, renvoi de l'attaque adverse.</p>
                    </div>
                  </div>
                  <img src="./public/img/portfolio/thumb-12.jpg" alt="" title="Portfolio Image 7" />
                </div>
              </div>
              <div class="s-12 m-6 l-4">
                <div class="image-with-hover-overlay image-hover-zoom margin-bottom">
                  <div class="image-hover-overlay background-primary">
                    <div class="image-hover-overlay-content text-center padding-2x">
                      <p>Découvrir cet art martial afro-brésilien.</p>
                    </div>
                  </div>
                  <img src="./public/img/portfolio/thumb-07.jpg" alt="" title="Portfolio Image 8" />
                </div>
              </div>
              <div class="s-12 m-6 l-4">
                <div class="image-with-hover-overlay image-hover-zoom margin-bottom">
                  <div class="image-hover-overlay background-primary">
                    <div class="image-hover-overlay-content text-center padding-2x">
                      <p>Le Karaté pratiqué est le "Shotokan".</p>
                    </div>
                  </div>
                  <img src="./public/img/portfolio/thumb-14.jpg" alt="" title="Portfolio Image 9" />
                </div>
              </div>
              <div class="s-12 m-6 l-4">
                <div class="image-with-hover-overlay image-hover-zoom margin-bottom">
                  <div class="image-hover-overlay background-primary">
                    <div class="image-hover-overlay-content text-center padding-2x">
                      <p>Le tango est une danse sociale née à la fin du xixe siècle et désigne le plus souvent une mesure à deux ou quatre temps plutôt marqués.</p>
                    </div>
                  </div>
                  <img src="./public/img/portfolio/thumb-13.jpg" alt="" title="Portfolio Image 10" />
                </div>
              </div>
              <div class="s-12 m-6 l-4">
                <div class="image-with-hover-overlay image-hover-zoom margin-bottom">
                  <div class="image-hover-overlay background-primary">
                    <div class="image-hover-overlay-content text-center padding-2x">
                      <p>TAEKWONDO SPORT ART MARTIAL COREEN.</p>
                    </div>
                  </div>
                  <img src="./public/img/portfolio/thumb-05.jpg" alt="" title="Portfolio Image 11" />
                </div>
              </div>
              <div class="s-12 m-6 l-4">
                <div class="image-with-hover-overlay image-hover-zoom margin-bottom">
                  <div class="image-hover-overlay background-primary">
                    <div class="image-hover-overlay-content text-center padding-2x">
                      <p>Les danses de salon sont les danses pratiquées dans les salons, bals, soirées, guinguettes et thés dansants.</p>
                    </div>
                  </div>
                  <img src="./public/img/portfolio/thumb-08.jpg" alt="" title="Portfolio Image 12" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </article>
    </main>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>