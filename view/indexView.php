<?php $title = 'Home'; ?>

<?php ob_start(); ?>

    <!-- MAIN -->
    <main role="main">
      <!-- Main Header -->
      <header>
        <div class="carousel-default owl-carousel carousel-main carousel-nav-white background-dark text-center">
          <div class="item">
            <div class="s-12">
              <img src="./public/img/header.jpg" alt="">
              <div class="carousel-content">
                <div class="content-center-vertical line">
                  <div class="margin-top-bottom-80">
                    <!-- Title -->
                    <h1 class="text-white margin-bottom-30 text-size-60 text-m-size-30 text-line-height-1"></h1>
                    <div class="s-12 m-10 l-8 center"><p class="text-white text-size-20 margin-bottom-">
                      <br>
                      <br><br>
                      <br><br>
                      <br>
                      <br>
                      <br>L'INTÉRÊT D'ORGANISER UN FORUM DES ASSOCIATIONS EN LIGNE.
                      <br>
                      <br>
                      Le forum des associations est un donc un endroit où les associations actives de votre ville se réuniront pour se présenter, promouvoir leurs activités, recruter de nouveaux membres.
                      <br>
                      <br>Cela pourra donner un aperçu aux anciens comme résidents, du paysage associatif de votre ville, dans sa globalité et de façon ludique.
                      <br>
                      <br>
  Le forum de associations est le lieu de convivialité idéal qui permet à chacun de rencontrer les autres acteurs de Maisons Alfort, d’une part, et d’autre part de vous présenter en tant qu’association.
  <br>
  <br>
  Les associations se font donc ainsi connaître des résidents, des institutions et des autres associations. <br>
  <br></p></div>
                    <div class="line">
                      <div class="s-12 m-12 l-3 center">
                        <a class="button button-white-stroke s-12" href="/">Commencer</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>