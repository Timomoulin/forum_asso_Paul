<?php $title = 'Contact'; ?>

<?php ob_start(); ?>

    <!-- MAIN -->
    <main role="main">
      <!-- Content -->
      <article>
        <header class="section background-dark">
          <div class="line">
            <h1 class="text-white margin-top-bottom-40 text-size-60 text-line-height-1">Contactez-nous</h1>
            <p class="margin-bottom-0 text-size-16">Pour composer votre programme loisirs de la rentrée 2021,
              <br>
              <br>
contactez-nous en ligne !

</p>
          </div>
        </header>
        <div class="section background-white">
          <div class="line margin-bottom-60">
            <div class="margin">
              <div class="s-12 m-12 l-4 margin-m-bottom">
                <div class="padding-2x block-bordered border-radius">
                  <div class="float-left hide-s">
                    <i class="icon-sli-location-pin icon3x text-primary"></i>
                  </div>
                <div class="margin-left-70 margin-s-left-0 margin-bottom">
                  <h3 class="margin-bottom-0">Mairie de Maisons Alfort</h3>
                  <p>118 Avenue du Général de Gaulle, 94700 Maisons-Alfort<br>

                  </p>
                </div>
                </div>
              </div>
              <div class="s-12 m-12 l-4 margin-m-bottom">
                <div class="padding-2x block-bordered border-radius">
                  <div class="float-left hide-s">
                    <i class="icon-sli-envelope icon3x text-primary"></i>
                  </div>
                  <div class="margin-left-70 margin-s-left-0 margin-bottom">
                    <h3 class="margin-bottom-0">E-mail</h3>
                    <p>ville@maisons-alfort.fr<br>

                    </p>
                  </div>
                </div>
              </div>
              <div class="s-12 m-12 l-4 margin-m-bottom">
                <div class="padding-2x block-bordered border-radius">
                  <div class="float-left hide-s">
                    <i class="icon-sli-phone icon3x text-primary"></i>
                  </div>
                  <div class="margin-left-70 margin-s-left-0">
                    <h3 class="margin-bottom-0">Téléphone</h3>
                    <p><span class="text-primary">Infoline :
                      </span><br>01 43 96 77 28
                      <br>

                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="line">
            <div class="margin">
              <!-- Contact Form -->
              <div class="s-12 m-12 l-6">
                <h2 class="margin-bottom-10">Laisser un message</h2>
                <form name="contactForm" class="customform" method="post">
                  <div class="line">
                    <div class="margin">
                      <div class="s-12 m-12 l-6">
                        <input name="email" class="email recquis" placeholder="e-mail" title="e-mail" type="text" />
                        <p class="email-error form-error">Votre e-mail.</p>
                      </div>
                      <div class="s-12 m-12 l-6">
                        <input name="nom" class="nom" placeholder="Nom" title="Nom" type="text" />
                        <p class="name-error form-error">Tapez votre nom.</p>
                      </div>
                    </div>
                  </div>
                  <div class="s-12">
                    <input name="sujet" class="sujet" placeholder="Sujet" title="Sujet" type="text" />
                    <p class="subject-error form-error">Tapez votre question.</p>
                  </div>
                  <div class="s-12">
                    <textarea name="message" class="message recquis" placeholder="Votre message" rows="3"></textarea>
                    <p class="message-error form-error">Tapez votre message.</p>
                  </div>
                  <div class="s-12"><button class="s-12 submit-form button background-primary text-white" type="submit">Soumettre</button></div>
                </form>
              </div>
              <div class="s-12 m-12 l-4">
                <h2 class="margin-bottom-10">Horaires</h2>
                <div class="s-6">
                  <p class="text-size-16">

                  </p>
                </div>
                <div class="s-6">
                  <p class="text-size-16 text-strong">
                  09h-17h<br>


                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </article>
    </main>
<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>