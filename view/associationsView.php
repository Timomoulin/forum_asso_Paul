<?php $title = 'Types d\'activités'; ?>

<?php ob_start(); ?>
 <!-- MAIN -->
 <main role="main">
      <!-- Content -->
      <article>
        <header class="section background-dark">
          <div class="line">
            <h1 class="text-white margin-top-bottom-40 text-size-60 text-line-height-1">Les Types d'activités</h1>
            <p class="margin-bottom-0 text-size-16">Un large choix d activités sportives et culturelles vous est proposé.</p>
          </div>
        </header>
        <section class="section background-white">
          <div class="line">
            <h2 class="text-size-40 margin-bottom-30">Types d'activités</h2>
            <hr class="break-small background-primary margin-bottom-30">
            <p class="margin-bottom-40">

            </p>
          </div>
          <div class="line text-center">
            <div class="margin">
          <?php 
    foreach($lesTypes as $unType)
    {
        foreach($unType->getActivités() as $uneActivite)
        {
            $chaineNomAct=($uneActivite->getNomActivité()." ");
        }
        $lienActivite="./?action=Activites&idType=".$unType->getNumTypeActivité();
       echo ('<div class="s-12 m-12 l-4 my-2 margin-m-bottom d-inline-block w-50">
       <div class="padding-2x background-primary ">
         <i class="icon-sli-home icon3x text-white margin-bottom-30"></i>
         <h2 class="text-thin">'.$unType->getNomTypeActivité().'</h2>
         <p class="margin-bottom-30">'.$chaineNomAct.'</p>
         <a class="button button-white-stroke text-size-12" href="'.$lienActivite.'">En savoir plus</a>
       </div>
     </div>');
    }
?>
          </div>
          </div>
        </section>
      </article>
    </main>




</div>
<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>