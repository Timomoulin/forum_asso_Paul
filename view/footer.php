 <!-- FOOTER -->
 <footer>
      <!-- Contact Us -->
      <div class="background-primary padding text-center">
        <p class="h1">Contactez-nous</p>
      </div>

      <!-- Main Footer -->
      <section class="background-dark full-width">
        <!-- Map -->
        <div class="s-12 m-12 l-6 margin-m-bottom-2x">
          <div class="s-12 grayscale center">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d10511.969469841815!2d2.4308336!3d48.8011243!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x57e8066403aef3d!2sMairie%20de%20Maisons-Alfort!5e0!3m2!1sfr!2sfr!4v1616539621501!5m2!1sfr!2sfr" width="400" height="300" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
          </div>
        </div>

        <!-- Collumn 2 -->
        <div class="s-12 m-12 l-6 margin-m-bottom-2x">
          <div class="padding-2x">
            <div class="line">
              <div class="float-left">
                  <i class="icon-sli-location-pin text-primary icon3x"></i>
                </div>
                <div class="margin-left-70 margin-bottom-30">
                  <h3 class="margin-bottom-0">Mairie de Maisons Alfort</h3>
                  <p>118 Avenue du Général de Gaulle, 94700 Maisons-Alfort<br>

                  </p>
                </div>
                <div class="float-left">
                  <i class="icon-sli-envelope text-primary icon3x"></i>
                </div>
                <div class="margin-left-70 margin-bottom-30">
                  <h3 class="margin-bottom-0">E-mail et site web</h3>
                  <p>ville@maisons-alfort.fr<br>
                     https://maisons-alfort.fr
                  </p>
                </div>
                <div class="float-left">
                  <i class="icon-sli-phone text-primary icon3x"></i>
                </div>
                <div class="margin-left-70">
                  <h3 class="margin-bottom-0">N° de téléphone</h3>
                  <p>01 43 96 77 28<br>
                  </p>
                </div>
            </div>
          </div>
        </div>
      </section>
      <hr class="break margin-top-bottom-0" style="border-color: rgba(0, 38, 51, 0.80);">

      <!-- Bottom Footer -->
      <section class="padding background-dark full-width">
        <div class="s-12 l-6">
          <p class="text-size-12">Copyright 2021 - Maisons Alfort</p>

        </div>

      </section>
    </footer>
    <script type="text/javascript" src="./public/js/responsee.js"></script>
    <script type="text/javascript" src="./public/owl-carousel/owl.carousel.js"></script>
    <script type="text/javascript" src="./public/js/template-scripts.js"></script>