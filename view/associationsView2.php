<?php $title = 'Associations'; ?>

<?php ob_start(); ?>
<main role="main">
<div class="container margin-top-bottom-70">

<h1>Associations</h1>
<?php
foreach($lesAssos as $uneAsso)
{
   echo ( '<div class="row my-2 p-2 col-lg-6">
        <h3>'.$uneAsso->getNomAssociation().'</h3>
        <ul>
            <li>Email : '.$uneAsso->getAdresseElectroniqueAssociation().'</li>
            <li>Site web :'.$uneAsso->getSiteAssociation().'</li>
        </ul>
    </div>');
}
?>

<br>
</div>
</div>
<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>