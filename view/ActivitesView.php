<?php $title = 'Activités'; ?>

<?php ob_start(); ?>
 <!-- MAIN -->
 <main role="main">
      <!-- Content -->
      <article>
        <header class="section background-dark">
          <div class="line">
            <h1 class="text-white margin-top-bottom-40 text-size-60 text-line-height-1">Activités</h1>
            <p class="margin-bottom-0 text-size-16">Un large choix d activités sportives et culturelles vous est proposé.</p>
          </div>
        </header>
        <section class="section background-white">
          <div class="line">
            <h2 class="text-size-40 margin-bottom-30">Activités</h2>
            <hr class="break-small background-primary margin-bottom-30">
            <p class="margin-bottom-40">

            </p>
          </div>
          <div class="line text-center">
            <div class="margin">
          <?php 
    foreach($lesActivites as $uneActivite)
    {
        $chaineNomAct="";
        // foreach($uneActivite->getAssociations() as $uneAsso)
        // {
        //     $chaineNomAct.=('<p class="margin-bottom-30">'.$uneAsso->getNomAssociation()."</p> ");
        // }
        $lienActivite="./?action=Associations&idActivite=".$uneActivite->getNumActivité();
       echo ('<div class="s-12 m-12 l-4 my-2 margin-m-bottom">
       <div class="padding-2x background-secondary">
         <i class="icon-sli-home icon3x text-white margin-bottom-30"></i>
         <h2 class="text-thin">'.$uneActivite->getNomActivité().'</h2>
         '.$chaineNomAct.'
         <a class="button button-white-stroke text-size-12" href="'.$lienActivite.'">En savoir plus</a>
       </div>
     </div>');
    }
?>
          </div>
          </div>
        </section>
      </article>
    </main>




</div>
<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>