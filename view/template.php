<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Forum Associations</title>
    <link rel="stylesheet" href="./public/css/components.css">
    <link rel="stylesheet" href="./public/css/icons.css">
    <link rel="stylesheet" href="./public/css/responsee.css">
    <link rel="stylesheet" href="./public/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="./public/owl-carousel/owl.theme.css">
    <link rel="stylesheet" href="./public/css/template-style.css">
    <link href='https://fonts.googleapis.com/css?family=Playfair+Display&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="./public/css/style.css">
    <script type="text/javascript" src="./public/js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="./public/js/jquery-ui.min.js"></script>
  </head>
    <!-- CSS bootstrap -->
    <link rel="stylesheet" href="./public/bootstrap-5.0.0-beta2-dist/css/bootstrap.css">
    <!-- CSS -->
    <link rel="stylesheet" href="./public/css/style.css">
    <title><?php echo $title; ?></title>
</head>
<body class="size-1140">
    <?php include "menu.php"; ?>
    <?php echo $content ; ?>
    <?php include "footer.php"; ?>
    <!-- JS bootstrap -->
    <script src="./public/bootstrap-5.0.0-beta2-dist/js/bootstrap.bundle.js"></script>
</body>
</html>