-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mer. 17 mars 2021 à 15:32
-- Version du serveur :  5.7.26
-- Version de PHP :  7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `forum_asso2`
--

-- --------------------------------------------------------

--
-- Structure de la table `activités`
--

DROP TABLE IF EXISTS `activités`;
CREATE TABLE IF NOT EXISTS `activités` (
  `NumActivité` int(11) NOT NULL AUTO_INCREMENT,
  `NomActivité` varchar(50) NOT NULL,
  `NumTypeActivité` int(11) NOT NULL,
  PRIMARY KEY (`NumActivité`),
  KEY `NumTypeActivité` (`NumTypeActivité`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `activités`
--

INSERT INTO `activités` (`NumActivité`, `NomActivité`, `NumTypeActivité`) VALUES
(1, 'Karaté', 1),
(2, 'Judo', 1),
(3, 'Self-Defense', 1),
(4, 'reunion anciens-combattants', 2),
(5, 'Découvertes culturelles et ludiques', 3),
(6, 'Loisirs et cultures (ado)', 3),
(7, 'Chorale', 4),
(8, 'Guitare', 4),
(9, 'Conservatoire', 4),
(10, 'Modèlisme', 5),
(11, 'Couture', 5),
(12, 'Geostratégie', 6),
(13, 'Espace/Astrologie', 6),
(14, 'Cours informatique', 7),
(15, 'Country', 8),
(16, 'West Coast Swing', 8),
(17, 'Athlétisme', 9),
(18, 'Pétanques', 9),
(19, 'Basket', 9);

-- --------------------------------------------------------

--
-- Structure de la table `associations`
--

DROP TABLE IF EXISTS `associations`;
CREATE TABLE IF NOT EXISTS `associations` (
  `NumAssociation` int(11) NOT NULL AUTO_INCREMENT,
  `NomAssociation` varchar(50) NOT NULL,
  `AssociationPrivée` tinyint(1) NOT NULL DEFAULT '1',
  `AdresseElectroniqueAssociation` varchar(50) DEFAULT NULL,
  `SiteAssociation` varchar(50) DEFAULT NULL,
  `Description` text,
  `NumActivité` int(11) DEFAULT NULL,
  PRIMARY KEY (`NumAssociation`),
  KEY `NumActivité` (`NumActivité`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `associations`
--

INSERT INTO `associations` (`NumAssociation`, `NomAssociation`, `AssociationPrivée`, `AdresseElectroniqueAssociation`, `SiteAssociation`, `Description`, `NumActivité`) VALUES
(1, 'Association Sportive Amicale(ASA KARATE)', 1, 'asa.karate.94@gmail.com', 'www.asakarate.fr', NULL, NULL),
(2, 'Association Sportive et Culturelle', 1, 'ascliberte@orange.fr', 'www.ascliberte.fr', NULL, NULL),
(3, 'Club Sportif et de Loisirs de la Gendarmerie ', 0, 'clubgend94@gmail.com', 'www.cslgma94.club.sportsregions.fr', NULL, NULL),
(4, 'Judo Club Maisons-Alfort', 1, NULL, 'www.jcma-judo.fr', NULL, NULL),
(5, 'Amicale des Anciens Marins et Marins Anciens Comba', 1, NULL, NULL, 'Réunir les anciens marins et marins anciens combattants et ceux qui s\'intéressent à la marine', NULL),
(6, 'AMICALE DES FRANÇAIS LIBRES DE MAISONS-ALFORT ', 1, NULL, NULL, 'Entretenir le devoir de mémoire des anciens Français Libres et des Compagnons de la Libération', NULL),
(7, 'CEUX DE CHERCHELL - ANCCORE ', 1, NULL, 'www.emicherchell.com', 'Cérémonie patriotique. Repas de cohésion. Création d\'un mémorial', NULL),
(8, 'OFFICE MUNICIPAL DE LA CULTURE-CSC La Croix des Ou', 0, 'omc.csccroixdesouches@gmail.com', 'www.omc-maisons-alfort.asso.fr', 'Activités culturelles et de loisirs sur des thématiques variées pour des enfants qui souhaitent créer, jouer, s\'exprimer et partager ', NULL),
(9, 'OFFICE MUNICIPAL DE LA CULTURE-MPT pompidou', 0, 'omc.mptpompidou@gmail.com', 'www.omc-maisons-alfort.asso.fr', 'Atelier p\'tits curieux. Découvertes culturelles et ludiques. Approche créative et thématique permettant à l\'enfant de mieux connaître son environnement et de s\'ouvrir aux autres ', NULL),
(10, 'OFFICE MUNICIPAL DE LA CULTURE-CSC Les Planètes', 0, 'omc.maisonsalfort@gmail.com', 'www.omc-maisons-alfort.asso.fr', 'Permettre aux jeunes ados d\'accéder à la culture et aux loisirs par le biais d\'activités sur le centre ou des sorties à thème ', NULL),
(11, 'ASSOCIATION CULTURELLE ET SPORTIVE DES PLANETES ', 1, 'acsplanetes@gmail.com', 'www.acsspmaisonsalfortcreteil.eu', NULL, NULL),
(12, 'BILBOQUET', 1, 'asc.94700@gmail.com', 'www.asc94700.fr', 'Ateliers collectifs et cours individuels. Méthode simple et efficace. A partir de 6 ans pour la guitare et 7 ans pour la basse ', NULL),
(13, 'CONSERVATOIRE HENRI DUTILLEUX CONSERVATOIRE A RAYO', 0, 'conservatoire.ville@maisons-alfort.fr', 'www.maisons-alfort.fr', 'INSTRUMENTS : à partir de 7 ans Accordéon, alto, clarinette et clarinette basse, flûte traversière, guitare (classique, électrique, jazz) harpe, percussion / batterie, piano, saxophone, trombone / tuba, trompette / cornet, violoncelle. Musiques actuelles accompagnement des groupes. EVEIL MUSICAL ET CORPOREL : à partir de 4 ans Se renseigner auprès du Conservatoire ', NULL),
(14, 'ARC-EN-CIEL CLUB DE MODELES REDUITS ', 1, 'aec.bateaux@gmail.com', 'arc-en-ciel-modelisme.org ', 'Reproduire à différentes échelles des bateaux, trains, voitures et les engager dans des concours, expositions ou tout simplement pour le plaisir ', NULL),
(15, 'ARTS LOISIRS ARTISANAT ', 1, NULL, NULL, NULL, NULL),
(16, 'Université Inter-Ages', 1, NULL, 'www.uia94.fr', NULL, NULL),
(17, 'OFFICE MUNICIPAL DE LA CULTURE-CSC Liberté', 0, 'omc.maisonsalfort@gmail.com', 'www.omc-maisons-alfort.asso.fr', 'Ateliers informatiques pour débuter ou pour approfondir. Proposition à l\'année de thématiques pour mieux appréhender l\'informatique', NULL),
(18, 'OFFICE MUNICIPAL DE LA CULTURE-MPT Pompidou', 0, 'omc.maisonsalfort@gmail.com', 'www.omc-maisons-alfort.asso.fr', 'Atelier découverte, initiation informatique et multimédia ', NULL),
(19, 'CLUB DE DANSE SPORTIVE DE MAISONS-ALFORT ', 1, 'cdsma94@gmail.com', 'www.cdsma.com', NULL, NULL),
(20, 'ASSOCIATION SPORTIVE AMICALE(ASA ATHLETISME) ', 1, 'asa.athletisme@free.fr', 'www.asa-athletisme.com', 'L\'ASA Athlétisme offre la possibilité de pratiquer toutes les disciplines et s\'adresse à tous les publics dès 5 ans, débutants comme confirmés ', NULL),
(21, 'ASSOCIATION SPORTIVE AMICALE (ASA BOULE LYONNAISE)', 1, 'asa-sport-boules-maisons-alfort@laposte.net', NULL, ' Jeu d\'adresse qui se pratique sur terrains tracés et aménagés, s\'exprime dans des situations de pointage et de tir en course ', NULL),
(22, 'ASSOCIATION DE LOISIRS ET DE PROXIMITE', 1, NULL, NULL, 'pratique de la petanque loisirs', NULL),
(23, 'ASSOCIATION SPORTIVE LIBERTE DE MAISONS-ALFORT', 1, 'zambe94@gmail.com', 'aslma.basket.fr', 'Promouvoir le basket ', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `avoirrole`
--

DROP TABLE IF EXISTS `avoirrole`;
CREATE TABLE IF NOT EXISTS `avoirrole` (
  `NumAssociation` int(11) NOT NULL,
  `NumContact` int(11) NOT NULL,
  `NumStatut` int(11) NOT NULL,
  PRIMARY KEY (`NumAssociation`,`NumContact`,`NumStatut`),
  KEY `NumContact` (`NumContact`),
  KEY `NumStatut` (`NumStatut`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `avoirrole`
--

INSERT INTO `avoirrole` (`NumAssociation`, `NumContact`, `NumStatut`) VALUES
(1, 1, 1),
(1, 2, 6),
(1, 3, 6),
(1, 4, 6),
(3, 6, 1),
(3, 7, 7),
(3, 8, 7),
(4, 11, 1),
(4, 12, 8),
(5, 14, 1),
(6, 15, 1),
(6, 16, 3),
(7, 17, 1),
(8, 18, 9),
(9, 19, 9),
(10, 20, 9),
(11, 22, 1),
(11, 23, 10),
(12, 26, 1),
(14, 27, 1),
(14, 28, 3),
(15, 29, 1),
(16, 30, 1),
(16, 31, 4),
(17, 32, 1);

-- --------------------------------------------------------

--
-- Structure de la table `contact`
--

DROP TABLE IF EXISTS `contact`;
CREATE TABLE IF NOT EXISTS `contact` (
  `NumContact` int(11) NOT NULL AUTO_INCREMENT,
  `CivilitéContact` varchar(50) NOT NULL,
  `NomContact` varchar(50) NOT NULL,
  `TelephoneContact` varchar(50) DEFAULT NULL,
  `AdresseElectroniqueContact` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`NumContact`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `contact`
--

INSERT INTO `contact` (`NumContact`, `CivilitéContact`, `NomContact`, `TelephoneContact`, `AdresseElectroniqueContact`) VALUES
(1, 'Monsieur', 'SENECHAL', '0685716753', NULL),
(2, 'Madame', 'ROUX', '0616959550', 'laurenceroux67@neuf.fr'),
(3, 'Monsieur', 'MIQUEL', '0626592777', NULL),
(4, 'Madame', 'MOLINA', '0603190793', NULL),
(5, 'Monsieur', 'SENECHAL', '0685716753', NULL),
(6, 'Monsieur', 'PLONEIS', '0157441425', NULL),
(7, 'Monsieur', 'PEREZ', '0663590769', NULL),
(8, 'Monsieur', 'DANG', '0661768999', NULL),
(9, 'Madame', 'CARRE', NULL, NULL),
(10, 'Monsieur', 'SENECHAL', '0685716753', NULL),
(11, 'Monsieur', 'GONON', '0667968986', 'marcgonon@neuf.fr'),
(12, 'Monsieur', 'HAMOT', '0668611872', NULL),
(13, 'Monsieur', 'SENECHAL', '0685716753', NULL),
(14, 'Monsieur', 'LAMORT', '0610641690', 'jeamarie.lamort@sfr.fr'),
(15, 'Monsieur', 'MICLO', '0143755735', NULL),
(16, 'Madame', 'ROULET', '0143764202', 'domisanti94@gmail.com'),
(17, 'Monsieur', 'TEIL', '0662000789', 'pitouteil@wanadoo.fr'),
(18, 'Madame', 'DEFOSSE', '0141791615', 'omc,cscroixdesouches@gmail.com'),
(19, 'Madame', 'DEFOSSE', '0141791965', 'omc.mptpomidou@gmail.com'),
(20, 'Monsieur', 'HADROUGA', '0142073868', 'omc.planetes2@wanadoo.fr'),
(21, 'Monsieur', 'SENECHAL', '0685716753', NULL),
(22, 'Madame', 'QUEUNIET', NULL, NULL),
(23, 'Madame', 'ROGER', '0685716753', NULL),
(24, 'Monsieur', 'SENECHAL', '0685716753', NULL),
(25, 'Autre', 'CONSERVATOIRE', '0153481017', NULL),
(26, 'Monsieur', 'VIDAL', '0660266440', 'pierre,vidal71@sfr.fr'),
(27, 'Monsieur', 'JOLLY', '0674538547', 'christian,jolly@gmail.fr'),
(28, 'Monsieur', 'MENGUE', NULL, 'jeanfrancois.mengue@free.fr'),
(29, 'Madame', 'STALETTI', '0682108107', 'staletti.lucette@orange.fr'),
(30, 'Madame', 'PASCHE', '0143780697', 'jeaninepasche@wanadoo.fr'),
(31, 'Madame', 'MAILFAIT', '0686765657', 'christinemail94@gmail.com'),
(32, 'Monsieur', 'KIFOUCHE', '0141790830', 'cscliberte@orange.fr'),
(33, 'Madame', 'DEFOSSE', '0141791965', 'omc.mptpomidou@gmail.fr'),
(34, 'Monsieur', 'IDDA', '0603107463', NULL),
(35, 'Monsieur', 'SUREAU', '0778351472', 'sureau_n@hotmail.fr'),
(36, 'Madame', 'SUREAU', '0781541333', NULL),
(37, 'Monsieur', 'GUEYE', '0670648526', NULL),
(38, 'Madame', 'NDEBEKA', '0616061222', 'cndebeka@gmail.com'),
(39, 'Monsieur', 'PALACIOS', '0620214943', NULL),
(40, 'Monsieur', 'SAMBA', '0758084325', 'sambamo@hotmail.fr');

-- --------------------------------------------------------

--
-- Structure de la table `horaires`
--

DROP TABLE IF EXISTS `horaires`;
CREATE TABLE IF NOT EXISTS `horaires` (
  `NumHoraire` int(11) NOT NULL AUTO_INCREMENT,
  `JoursSemaine` varchar(50) NOT NULL,
  `HeureDébut` time DEFAULT NULL,
  `HeureFin` time DEFAULT NULL,
  PRIMARY KEY (`NumHoraire`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `horaires`
--

INSERT INTO `horaires` (`NumHoraire`, `JoursSemaine`, `HeureDébut`, `HeureFin`) VALUES
(1, 'MERCREDI', '17:30:00', '18:30:00'),
(2, 'MERCREDI', '18:30:00', '19:30:00'),
(3, 'MERCREDI', '19:30:00', '21:30:00'),
(4, 'VENDREDI', '18:00:00', '19:00:00'),
(5, 'VENDREDI', '19:00:00', '21:30:00'),
(6, 'SAMEDI', '12:00:00', '13:00:00'),
(7, 'LUNDDI', '19:30:00', '21:00:00'),
(8, 'SAMEDI', '15:00:00', '18:30:00'),
(9, 'DIMANCHE', '10:00:00', '12:30:00'),
(10, 'DIMANCHE', '14:30:00', '17:00:00'),
(11, 'MERCREDI', '15:30:00', '16:30:00'),
(12, 'MERCREDI', '16:30:00', '17:30:00'),
(13, 'JEUDI', '16:00:00', '18:00:00'),
(14, 'JEUDI', '20:00:00', '22:00:00'),
(15, 'LUNDI', '14:00:00', '17:00:00'),
(16, 'VENDREDI', '09:30:00', '11:30:00'),
(17, 'LUNDI', '14:00:00', '16:30:00'),
(18, 'LUNDI', '20:00:00', '22:00:00'),
(19, 'VENDREDI', '19:30:00', '20:30:00'),
(20, 'VENDREDI', '20:30:00', '21:30:00'),
(21, 'VENDREDI', '21:30:00', '22:30:00'),
(22, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `lieux`
--

DROP TABLE IF EXISTS `lieux`;
CREATE TABLE IF NOT EXISTS `lieux` (
  `NumLieu` int(11) NOT NULL AUTO_INCREMENT,
  `NomLieu` varchar(50) NOT NULL,
  `AdresseLieu` varchar(50) NOT NULL,
  PRIMARY KEY (`NumLieu`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `lieux`
--

INSERT INTO `lieux` (`NumLieu`, `NomLieu`, `AdresseLieu`) VALUES
(1, 'DOJO GYMNASE POMPIDOU', '12, rue Georges Gaumé'),
(2, 'GYMNASE CONDORCET', '35, rue Danielle Casanova'),
(3, 'GYMNASE HERBERT', '79, avenue de la Liberté'),
(4, 'ASC', '58, rue Chevreuil'),
(5, 'DOJO QUARTIER MOHIER', '24, avenue Busteau'),
(6, 'MAISON DU COMBATTANT', '27, rue Jouët'),
(7, 'CSC LA CROIX DES OUCHES', '33, avenue de la République'),
(8, 'MPT POMPIDOU', '12,rue Georges Gaumé'),
(9, 'CSC LES PLANETES', '149, rue Marc Sangnier'),
(10, 'ESPACE JEAN FERRAT', '21, rue Charles Beuvin'),
(11, 'MPT D ALFORT', '1, rue Maréchal Juin'),
(12, 'ESPACE MULTIMEDIA', '11 bis, Square Dufourmantelle'),
(13, 'STADE DELAUNE', '61, rue du 11 Novembre 1918'),
(14, 'BOULODROME STADE CUBIZOLLES', '25 bis, avenuedu Général de Gaulle'),
(15, 'GYMNASE PARMENTIER', '8, rue Paul Vaillant Couturier');

-- --------------------------------------------------------

--
-- Structure de la table `pratiquer`
--

DROP TABLE IF EXISTS `pratiquer`;
CREATE TABLE IF NOT EXISTS `pratiquer` (
  `NumAssociation` int(11) NOT NULL,
  `NumLieu` int(11) NOT NULL,
  `NumHoraire` int(11) NOT NULL,
  `TypePublic` varchar(50) DEFAULT NULL,
  `Details` text,
  PRIMARY KEY (`NumAssociation`,`NumLieu`,`NumHoraire`),
  KEY `NumLieu` (`NumLieu`),
  KEY `NumHoraire` (`NumHoraire`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `pratiquer`
--

INSERT INTO `pratiquer` (`NumAssociation`, `NumLieu`, `NumHoraire`, `TypePublic`, `Details`) VALUES
(2, 4, 1, 'de 6 a 9 ans', NULL),
(2, 4, 2, 'de 9 a 14 ans', NULL),
(2, 4, 3, 'adultes', NULL),
(2, 4, 4, 'enfants et ados', NULL),
(2, 4, 5, 'adultes', NULL),
(2, 4, 6, 'adultes', NULL),
(3, 5, 8, 'des 10 ans et adultes', NULL),
(3, 5, 9, 'des 10 ans et adultes', NULL),
(3, 5, 10, 'des 14 ans et adultes', NULL),
(3, 5, 11, 'de 4 a 5 ans ', NULL),
(3, 5, 12, 'de 6 a 7 ans ', NULL),
(5, 15, 13, NULL, '1er jeudi du mois'),
(15, 7, 15, 'tout ages', NULL),
(16, 15, 16, NULL, NULL),
(16, 15, 17, 'seniors', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `proposer`
--

DROP TABLE IF EXISTS `proposer`;
CREATE TABLE IF NOT EXISTS `proposer` (
  `NumActivité` int(11) NOT NULL,
  `NumAssociation` int(11) NOT NULL,
  PRIMARY KEY (`NumActivité`,`NumAssociation`),
  KEY `NumAssociation` (`NumAssociation`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `proposer`
--

INSERT INTO `proposer` (`NumActivité`, `NumAssociation`) VALUES
(1, 1),
(1, 2),
(3, 2),
(2, 3),
(3, 3),
(2, 4),
(4, 5),
(4, 6),
(4, 7),
(5, 8),
(5, 9),
(6, 10),
(7, 11),
(15, 11),
(8, 12),
(9, 13),
(10, 14),
(11, 15),
(12, 16),
(13, 16),
(14, 17),
(14, 18),
(16, 19),
(17, 20),
(18, 21),
(18, 22),
(19, 23);

-- --------------------------------------------------------

--
-- Structure de la table `statut`
--

DROP TABLE IF EXISTS `statut`;
CREATE TABLE IF NOT EXISTS `statut` (
  `NumStatut` int(11) NOT NULL AUTO_INCREMENT,
  `NomStatut` varchar(50) NOT NULL,
  PRIMARY KEY (`NumStatut`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `statut`
--

INSERT INTO `statut` (`NumStatut`, `NomStatut`) VALUES
(1, 'Président'),
(2, 'Trésorière'),
(3, 'Vice-Président'),
(4, 'Secrétaire'),
(5, 'Secrétaire Général'),
(6, 'Professeur'),
(7, 'Animateur de Section'),
(8, 'Responsable'),
(9, 'Directeur'),
(10, 'Membre du CA');

-- --------------------------------------------------------

--
-- Structure de la table `typeactivité`
--

DROP TABLE IF EXISTS `typeactivité`;
CREATE TABLE IF NOT EXISTS `typeactivité` (
  `NumTypeActivité` int(11) NOT NULL AUTO_INCREMENT,
  `NomTypeActivité` varchar(50) NOT NULL,
  PRIMARY KEY (`NumTypeActivité`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `typeactivité`
--

INSERT INTO `typeactivité` (`NumTypeActivité`, `NomTypeActivité`) VALUES
(1, 'Sports de Combats'),
(2, 'Anciens Combattants'),
(3, 'Ateliers Jeunes/Enfants'),
(4, 'Musique'),
(5, 'Activités Manuelles'),
(6, 'Culture'),
(7, 'Informatique'),
(8, 'Danse'),
(9, 'Sports');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `activités`
--
ALTER TABLE `activités`
  ADD CONSTRAINT `activités_ibfk_1` FOREIGN KEY (`NumTypeActivité`) REFERENCES `typeactivité` (`NumTypeActivité`);

--
-- Contraintes pour la table `associations`
--
ALTER TABLE `associations`
  ADD CONSTRAINT `associations_ibfk_1` FOREIGN KEY (`NumActivité`) REFERENCES `activités` (`NumActivité`);

--
-- Contraintes pour la table `avoirrole`
--
ALTER TABLE `avoirrole`
  ADD CONSTRAINT `avoirrole_ibfk_1` FOREIGN KEY (`NumAssociation`) REFERENCES `associations` (`NumAssociation`),
  ADD CONSTRAINT `avoirrole_ibfk_2` FOREIGN KEY (`NumContact`) REFERENCES `contact` (`NumContact`),
  ADD CONSTRAINT `avoirrole_ibfk_3` FOREIGN KEY (`NumStatut`) REFERENCES `statut` (`NumStatut`);

--
-- Contraintes pour la table `pratiquer`
--
ALTER TABLE `pratiquer`
  ADD CONSTRAINT `pratiquer_ibfk_1` FOREIGN KEY (`NumAssociation`) REFERENCES `associations` (`NumAssociation`),
  ADD CONSTRAINT `pratiquer_ibfk_2` FOREIGN KEY (`NumLieu`) REFERENCES `lieux` (`NumLieu`),
  ADD CONSTRAINT `pratiquer_ibfk_3` FOREIGN KEY (`NumHoraire`) REFERENCES `horaires` (`NumHoraire`);

--
-- Contraintes pour la table `proposer`
--
ALTER TABLE `proposer`
  ADD CONSTRAINT `proposer_ibfk_1` FOREIGN KEY (`NumActivité`) REFERENCES `activités` (`NumActivité`),
  ADD CONSTRAINT `proposer_ibfk_2` FOREIGN KEY (`NumAssociation`) REFERENCES `associations` (`NumAssociation`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
