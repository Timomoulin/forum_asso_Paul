<?php

require('model/bdd.php');

function page1()
{
   
    require('view/indexView.php');
}

function voirActivites($idType)
{
    $uneBDD= new ActiviteManager();
    $lesActivites=$uneBDD->getActivitesBy("NumTypeActivité",$idType);
    require("view/ActivitesView.php");
}

function voirAssociations2($idActivite)
{
    $uneBDD= new AssociationManager();
    $lesAssos=$uneBDD->getAssociationBy("NumActivité",$idActivite);
    require("view/associationsView2.php");
}

function voirAssociations()
{
    $uneBDD= new TypeManager();
    $lesTypes= $uneBDD->getAllTypes();
    require("view/associationsView.php");
}

function  voirAPropos()
{
    require("view/aboutView.php");
}

function voirContact()
{
    require("view/contactView.php");
}
function voirGallery()
{
    require("view/galleryView.php");
}

?>