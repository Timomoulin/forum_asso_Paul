<?php
require ("classes/Type.class.php");
require ("classes/Activite.class.php");
require ("classes/Association.class.php");
require_once ('managers/TypeManager.php');
require_once ('managers/ActiviteManager.php');
require_once ('managers/AssociationManager.php');
class BDD
{
    public $connex;

    function __construct()
    {
        $this->connex= new PDO("mysql:host=localhost;dbname=forum_asso", "root", "",array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
        $this->connex->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    // function getAllTypes()
    // {
    //     try{
    //         $sql= $this->connex->prepare("SELECT * FROM typeactivité");
    //         $sql->setFetchMode(PDO::FETCH_CLASS, 'Type');
    //         $sql->execute();
            
    //         $lesTypes=array();
    //         $resultat=($sql->fetchAll());
            
    //         foreach($resultat as $unType)
    //         {
    //             $lesActivites=$this->getActivitesBy("NumTypeActivité",$unType->getNumTypeActivité());
    //             $unType->setActivités($lesActivites);
    //             array_push($lesTypes,$unType); // Ajouter un type a la liste de types
    //         }
    //         return $lesTypes;
           
    //     }
    //     catch(PDOException $error)
    //     {
    //         echo $error->getMessage();
    //     }
    // }
    // function getTypesBy($nomCol,$valeurCol)
    // {
    //     try{
    //         $sql= $this->connex->prepare("SELECT * FROM typeactivité WHERE $nomCol = :valeurCol");
    //         $sql->setFetchMode(PDO::FETCH_CLASS, 'Type');
    //         $sql->bindParam(":valeurCol",$valeurCol);
    //         $sql->execute();
            
           
    //         $resultat=($sql->fetchAll());
            
    //         foreach($resultat as $unType)
    //         {
    //             $lesActivites=$this->getActivitesBy("NumTypeActivité",$unType->getNumTypeActivité());
    //             $unType->setActivités($lesActivites);
    //         }
    //         return $resultat;
           
    //     }
    //     catch(PDOException $error)
    //     {
    //         echo $error->getMessage();
    //     }
    // }
    // function addTypes($nomType)
    // {
    //     try{
    //         $sql= $this->connex->prepare("INSERT INTO typeActivité values(null,:nomType)");
    //         $sql->bindParam(":nomType",$nomType);
    //         $sql->execute();
    //         //Verification de l'ajout
    //         $newId=$this->connex->lastInsertId(); // Permet de recuperer l'id du dernier ajout
    //         $verifType=$this->getTypesBy("NumTypeActivité",$newId)[0]; // Extraction du dernier ajout
    //         if($verifType->getNomTypeActivité()==$nomType) // Verification que le nom du dernier ajout==$nomType
    //         {
    //             return true; //Ajout OK
    //         }
    //         else {
    //             return false; //Ajout a échoué
    //         }
    //     }
    //     catch(PDOException $error)
    //     {
    //         echo $error->getMessage();
    //     }
    // }
    // function updateTypes($idType,$nomType)
    // {
    //     {
    //         try{
    //             $sql= $this->connex->prepare("UPDATE typeActivité SET NomTypeActivité = :nomType where NumTypeActivité=:uneId");
    //             $sql->bindParam(":nomType",$nomType);
    //             $sql->bindParam(":uneId",$idType);
    //             $sql->execute();
    //             //Verification
    //             $verifType=$this->getTypesBy("NumTypeActivité",$idType)[0]; // Extraction du Type
    //             if($verifType->getNomTypeActivité()==$nomType) // Verification que le nom du dernier ajout==$nomType
    //             {
    //                 return true; //Modification OK
    //             }
    //             else {
    //                 return false; //Modification a échoué
    //             }
    //         }
    //         catch(PDOException $error)
    //         {
    //             echo $error->getMessage();
    //         }
    //     }
    // }
    // function deleteTypes($idType)
    // {
    //     try{
    //         $leType=$this->getTypesBy("NumTypeActivité",$idType)[0];
    //         foreach($leType->getActivités() as $activité)
    //         {
    //             var_dump($this->deleteActivites($activité->getNumActivité()));
    //         }
    //         $sql2= $this->connex->prepare("DELETE FROM typeActivité where NumTypeActivité=:uneId");
    //         $sql2->bindParam(":uneId",$idType);
    //         var_dump($sql2);
    //         $sql2->execute();
    //         //Verification
    //         $verifType=$this->getTypesBy("NumTypeActivité",$idType); // Extraction du Type
    //         if(count($verifType)==0) // Verification 
    //         {
    //             return true; //Modification OK
    //         }
    //         else {
    //             return false; //Modification a échoué
    //         }
    //     }
    //     catch(PDOException $error)
    //     {
    //         echo $error->getMessage();
    //     }
    // }
    
    // function getActivitesBy($nomCol,$valeurCol)
    // {
    //     try{
    //         $sql= $this->connex->prepare("SELECT * FROM activités where $nomCol =:valeurCol");
    //         $sql->setFetchMode(PDO::FETCH_CLASS, 'Activite');
    //         $sql->bindParam(":valeurCol",$valeurCol);
    //         $sql->execute();
    //         $resultat=($sql->fetchAll());
    //         foreach($resultat as $uneActivité )
    //         {
    //             $lesAssos=$this->getAssociationBy("NumActivité",$uneActivité->getNumActivité());
    //             $uneActivité->setAssociations($lesAssos);
    //         }
    //         return $resultat;
           
    //     }
    //     catch(PDOException $error)
    //     {
    //         echo $error->getMessage();
    //     }
    // }
    // function addActivites($nom,$idType)
    // {
    //     try{
    //         $sql= $this->connex->prepare("INSERT INTO activités values(null,:nom,:idType)");
    //         $sql->bindParam(":nom",$nom);
    //         $sql->bindParam(":idType",$idType);
    //         $sql->execute();
    //         //Verification de l'ajout
    //         $newId=$this->connex->lastInsertId(); // Permet de recuperer l'id du dernier ajout
    //         $verif=$this->getActivitesBy("NumActivité",$newId)[0]; // Extraction du dernier ajout
    //         if($verif->getNomActivité()==$nom) // Verification que le nom du dernier ajout==$nomType
    //         {
    //             return true; //Ajout OK
    //         }
    //         else {
    //             return false; //Ajout a échoué
    //         }
    //     }
    //     catch(PDOException $error)
    //     {
    //         echo $error->getMessage();
    //     }
    // }
    // function updateActivites($id,$nom)
    // {
    //     {
    //         try{
    //             $sql= $this->connex->prepare("UPDATE activités SET NomActivité = :nom where NumActivité=:uneId");
    //             $sql->bindParam(":nom",$nom);
    //             $sql->bindParam(":uneId",$id);
    //             $sql->execute();
    //             //Verification de la modif
    //             $verif=$this->getActivitesBy("NumActivité",$id)[0]; // Extraction de l'activite
    //             if($verif->getNomActivité()==$nom) // Verification que le nom du dernier ajout==$nom
    //             {
    //                 return true; //Modification OK
    //             }
    //             else {
    //                 return false; //Modification a échoué
    //             }
    //         }
    //         catch(PDOException $error)
    //         {
    //             echo $error->getMessage();
    //         }
    //     }
    // }
    // function deleteActivites($id)
    // {
    //     try{
    //         $sql1= $this->connex->prepare("DELETE FROM proposer where NumActivité=:uneId");
    //         $sql1->bindParam(":uneId",$id);
    //         $sql1->execute();
            
    //         $sql2= $this->connex->prepare("DELETE FROM activités where NumActivité=:uneId");
    //         $sql2->bindParam(":uneId",$id);
    //         $sql2->execute();
            
    //         //Verification de l'ajout
    //         $verif=$this->getActivitesBy("NumActivité",$id); // Extraction du Type
    //         if(count($verif)==0) // Verification 
    //         {
    //             return true; //Modification OK
    //         }
    //         else {
    //             return false; //Modification a échoué
    //         }
    //     }
    //     catch(PDOException $error)
    //     {
    //         echo $error->getMessage();

    //     }
    // }
    // // function getAssociationBy($nomCol,$valeurCol)
    // // {
    // //     try{
    // //         // SELECT proposer.NumAssociation,`NomAssociation`,`AssociationPrivée`,`AdresseElectroniqueAssociation`,`SiteAssociation`,`Description`,proposer.numActivité FROM associations INNER JOIN proposer on proposer.NumAssociation=associations.NumAssociation where `NumActivité` =1
    // //         $sql= $this->connex->prepare("SELECT * FROM associations NATURAL join proposer where $nomCol =:valeurCol");
    // //         $sql->setFetchMode(PDO::FETCH_CLASS,'Association');
    // //         $sql->bindParam(":valeurCol",$valeurCol);
    // //         $sql->execute();
    // //         $resultat=($sql->fetchAll());
    // //         //TODO 
    // //         return $resultat;
           
    // //     }
    // //     catch(PDOException $error)
    // //     {
    // //         echo $error->getMessage();
    // //     }
    // // }
    // function getAssociationBy($nomCol,$valeurCol)
    // {
    //     try{
    //     $sql = $this->connex->prepare(" Select * from associations natural join proposer  where $nomCol=:valeurCol");
    //     $sql->setFetchMode(PDO::FETCH_CLASS, 'Association');
    //     $sql->bindParam(':valeurCol', $valeurCol);
    //     $sql->execute(); 
    //     $resultat = ($sql->fetchAll());
      
    //    return $resultat;  
    //     }
    //     catch(PDOException $error)
    //     {
    //         echo $error->getMessage();
    //     }

    // }
    // function updateAssociations($num,$nom,$prive,$email,$site,$description)
    // {
    //     {
    //         try{
    //             $sql= $this->connex->prepare("UPDATE associations SET NomAssociation = :nom,AssociationPrivée=:prive2, AdresseElectroniqueAssociation=:email, SiteAssociation=:site,Description=:description where NumAssociation=:num");
    //             $sql->bindParam(":nom",$nom);
    //             $sql->bindParam(":num",$num);
    //             $sql->bindParam(":prive2",$prive);
    //             $sql->bindParam(":email",$email);
    //             $sql->bindParam(":site",$site);
    //             $sql->bindParam(":description",$description);
    //             $sql->execute();
    //             //Verification de la modif
    //             $verif=$this->getAssociationBy("NumAssociation",$num)[0]; // Extraction de l'activite
    //             if($verif->getNomActivité()==$nom) // Verification que le nom du dernier ajout==$nom
    //             {
    //                 return true; //Modification OK
    //             }
    //             else {
    //                 return false; //Modification a échoué
    //             }
    //         }
    //         catch(PDOException $error)
    //         {
    //             echo $error->getMessage();
    //         }
    //     }
    // }
    //TODO 1 : Faire une classe association dans le dossier classes prendre comme (voir les autres classe comme exemple)
    //TODO 2 : Avant la ligne 4 ajouter un require pour la classe association
    //TODO 3 : Créer une méthode getAssociationsBy dans la classe BDD similaire a getActivitesBy mais avec un innerJoin proposer
    //TODO 4 : Modifier getActivitesBy pour ajouter les associations a l'activité ( comme a fait pour les types et activités dans getAllTypes )
    //TODO 5 : récuperer toutes les infos des assos qui sont dans les autres tables 
}

// $uneBDD=new BDD();
// $lesAssos=$uneBDD->getAssociationBy("NumActivité",4)[0];
// var_dump($lesAssos);
// $lesTypes=$uneBDD->getAllTypes();
// var_dump($lesTypes);
// $lesActivites=$uneBDD->getActivitesBy("NumTypeActivité",4);
// var_dump($lesActivites);
// var_dump($uneBDD->addTypes("TestType"));
?>