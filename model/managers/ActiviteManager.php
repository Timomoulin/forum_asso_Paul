<?php
require_once("AssociationManager.php");
class ActiviteManager extends BDD
{
    private $assoManager;
    function __construct()
    {
        parent::__construct();
        $this->assoManager = new AssociationManager();
    }
    function getActivitesBy($nomCol, $valeurCol)
    {
        try {
            $sql = $this->connex->prepare("SELECT * FROM activités where $nomCol =:valeurCol");
            $sql->setFetchMode(PDO::FETCH_CLASS, 'Activite');
            $sql->bindParam(":valeurCol", $valeurCol);
            $sql->execute();
            $resultat = ($sql->fetchAll());
            foreach ($resultat as $uneActivité) {

                $lesAssos = $this->assoManager->getAssociationBy("NumActivité", $uneActivité->getNumActivité());
                $uneActivité->setAssociations($lesAssos);
            }
            return $resultat;
        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }
    function addActivites($nom, $idType)
    {
        try {
            $sql = $this->connex->prepare("INSERT INTO activités values(null,:nom,:idType)");
            $sql->bindParam(":nom", $nom);
            $sql->bindParam(":idType", $idType);
            $sql->execute();
            //Verification de l'ajout
            $newId = $this->connex->lastInsertId(); // Permet de recuperer l'id du dernier ajout
            $verif = $this->getActivitesBy("NumActivité", $newId)[0]; // Extraction du dernier ajout
            if ($verif->getNomActivité() == $nom) // Verification que le nom du dernier ajout==$nomType
            {
                return true; //Ajout OK
            } else {
                return false; //Ajout a échoué
            }
        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }
    function updateActivites($id, $nom)
    { {
            try {
                $sql = $this->connex->prepare("UPDATE activités SET NomActivité = :nom where NumActivité=:uneId");
                $sql->bindParam(":nom", $nom);
                $sql->bindParam(":uneId", $id);
                $sql->execute();
                //Verification de la modif
                $verif = $this->getActivitesBy("NumActivité", $id)[0]; // Extraction de l'activite
                if ($verif->getNomActivité() == $nom) // Verification que le nom du dernier ajout==$nom
                {
                    return true; //Modification OK
                } else {
                    return false; //Modification a échoué
                }
            } catch (PDOException $error) {
                echo $error->getMessage();
            }
        }
    }
    function deleteActivites($id)
    {
        try {
            $sql1 = $this->connex->prepare("DELETE FROM proposer where NumActivité=:uneId");
            $sql1->bindParam(":uneId", $id);
            $sql1->execute();

            $sql2 = $this->connex->prepare("DELETE FROM activités where NumActivité=:uneId");
            $sql2->bindParam(":uneId", $id);
            $sql2->execute();

            //Verification de l'ajout
            $verif = $this->getActivitesBy("NumActivité", $id); // Extraction du Type
            if (count($verif) == 0) // Verification 
            {
                return true; //Modification OK
            } else {
                return false; //Modification a échoué
            }
        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }
}
