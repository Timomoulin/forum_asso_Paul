<?php
class AssociationManager extends BDD
{

    function getAssociationBy($nomCol, $valeurCol)
    {
        try {
            $sql = $this->connex->prepare(" Select * from associations natural join proposer  where $nomCol=:valeurCol");
            $sql->setFetchMode(PDO::FETCH_CLASS, 'Association');
            $sql->bindParam(':valeurCol', $valeurCol);
            $sql->execute();
            $resultat = ($sql->fetchAll());

            return $resultat;
        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }
    function updateAssociations($num, $nom, $prive, $email, $site, $description)
    { {
            try {
                $sql = $this->connex->prepare("UPDATE associations SET NomAssociation = :nom,AssociationPrivée=:prive2, AdresseElectroniqueAssociation=:email, SiteAssociation=:site,Description=:description where NumAssociation=:num");
                $sql->bindParam(":nom", $nom);
                $sql->bindParam(":num", $num);
                $sql->bindParam(":prive2", $prive);
                var_dump($prive);
                $sql->bindParam(":email", $email);
                $sql->bindParam(":site", $site);
                $sql->bindParam(":description", $description);
                $sql->execute();
                //Verification de la modif
                $verif = $this->getAssociationBy("NumAssociation", $num)[0]; // Extraction de l'activite
                if ($verif->getNomAssociation() == $nom) // Verification que le nom du dernier ajout==$nom
                {
                    return true; //Modification OK
                } else {
                    return false; //Modification a échoué
                }
            } catch (PDOException $error) {
                echo $error->getMessage();
            }
        }
    }
}
