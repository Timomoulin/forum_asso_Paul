<?php
require('controller/controller.php');
require('controller/gestionnaireController.php');
/* index.php Est le routeur il va :
*   - Regarder quelle est la valeur du param action
*   - Generalement il vas allez chercher des données dans le model
*   - Et utiliser invoquer (appeler) une fonction du controlleur en lui passant des données (du model) si besoin
*/
try{
    if (isset($_GET['action'])) {
        $action=test_input($_GET['action']);
        if ($action == 'Types') {
            voirAssociations();
        }
        else if($action=="Activites"&&isset($_GET['idType']))
        {
            $idType=test_input($_GET['idType']);
            voirActivites($idType);
        }
        elseif ($action=="Associations"&&isset($_GET['idActivite']))
        {
            $idActivite=test_input($_GET['idActivite']);
            voirAssociations2($idActivite);
        }
        elseif ($action=="about")
        {
            voirAPropos();
        }
        elseif ($action=="contact")
        {
            voirContact();
        }
        elseif ($action=="gallery")
        {
            voirGallery();
        }
        else if($action=="FormulaireAjoutType")
        {
            formulaireAjoutType();
        }
        else if($action=="FormulaireModifType"&&isset($_GET['id']))
        {
            $id=test_input($_GET['id']);
            fomulaireModifType($id);
        }
        else if($action=="resultat"&&isset($_GET['operation'])&&isset($_GET['valeur']))
        {
            $operation=test_input($_GET['operation']);
            $valeur=test_input($_GET['valeur']);
            resultatOperation($operation,$valeur);
        }
        else if($action=='addTypes'&&isset($_POST['nomType']))
        {
            
            $nom=test_input($_POST['nomType']);
            addTypes($nom);
        }
        else if($action=='updateTypes'&&isset($_POST['nomType'])&&isset($_POST['idType']))
        {
            $nom=test_input($_POST['nomType']);
            $id=test_input($_POST['idType']);
            updateTypes($id,$nom);
        }
        else if($action=="deleteTypes"&&isset($_GET['id']))
        {
            $id=test_input($_GET['id']);
            deleteTypes($id);
        }
        
        else if($action=="FormulaireAjoutActivites"&&isset($_GET['idType']))
        {
            $idType=test_input($_GET['idType']);
            formulaireAjoutActivite($idType);
        }
        else if($action=="FormulaireModifActivites"&&isset($_GET['id']))
        {
            $idActivite=test_input($_GET['id']);
            formulaireModificationActivite($idActivite);
        }
        
        else if($action=="addActivites"&&isset($_POST['idType'])&&isset($_POST['nomActivite']))
        {
            $idType=test_input($_POST['idType']);
            $nomActivite=test_input($_POST['nomActivite']);
            addActivites($nomActivite,$idType);
        }
        
        else if($action=="updateActivites"&&isset($_POST['idActivite'])&&isset($_POST['nomActivite']))
        {
            $idActivite=test_input($_POST['idActivite']);
            $nom=test_input($_POST['nomActivite']);
            updateActivites($idActivite,$nom);
        }
        else if($action=="deleteActivites"&&isset($_GET['id']))
        {
            $id=test_input($_GET['id']);
            deleteActivites($id);
        }
        elseif ($action=="FormulaireModificationAssociations"&&isset($_GET['id']))
        {
            $id=test_input($_GET['id']);
            formulaireModificationAssociation($id);
        }
        else{
            throw new Exception ("Pas de page pour cette action");
        }
    }
    else {
        page1();
    }
}
catch(Exception $e){
    echo 'Erreur : ' . $e->getMessage();
}
function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
  }
?>